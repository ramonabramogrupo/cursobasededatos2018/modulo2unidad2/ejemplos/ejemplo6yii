<?php

use yii\db\Migration;

/**
 * Class m190408_081922_autores
 */
class m190408_081922_autores extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable("autores", [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(100),
            'foto' => $this->string(25),
        ]);

        $this->insert("autores", [
            'nombre' => 'Miguel de Cervantes',
            'foto' => 'autor1.jpg',
        ]);

        $this->insert("autores", [
            'nombre' => 'Marcel Proust',
            'foto' => 'autor2.jpg',
        ]);

        $this->insert("autores", [
            'nombre' => 'Gustavo Adolfo Bequer',
            'foto' => 'autor3.jpg',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {

        $this->dropTable('autores');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190408_081922_autores cannot be reverted.\n";

      return false;
      }
     */
}
