<?php

use yii\db\Migration;

/**
 * Class m190408_082054_libros
 */
class m190408_082054_libros extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("libros", [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(100),
            'editorial' => $this->string(25),
            'autor'=>$this->integer(),
            'portada'=>$this->string(25),
        ]);

        $this->addForeignKey('fklibros_autores', 'libros', 'autor', 'autores',
                'id', 'cascade', 'cascade');

        $this->insert("libros", [
            'nombre' => 'El licenciado Vidriera',
            'editorial'=> 'Planeta',
            'autor'=> 1,
            'portada' => 'portada1.jpg',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190408_082054_libros cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190408_082054_libros cannot be reverted.\n";

        return false;
    }
    */
}
