<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Ejemplo 6 de Yii</h1>

        <p class="lead">Gestion de Libros</p>
    </div>

    <div class="btn-group-vertical" role="group" aria-label="...">
        <a href="<?= Url::to(['autores/listar']) ?>" class="btn btn-large btn-primary"> Autores </a>
        <?= Html::a('Libros', ['libros/listar'], ['class' => 'btn btn-large btn-primary']); ?>
    </div>

</div>
