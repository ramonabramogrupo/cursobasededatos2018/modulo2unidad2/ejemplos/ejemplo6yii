<?php

use yii\grid\GridView;

?>

<h1> <?= $nombre ?> </h1>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'nombre',
            'editorial',
            'autor',
            'portada',
        ],
    ]); 

?>

<div class="row">
    <div class="col-sm-12">
        <?= \yii\helpers\Html::a( 'Atras', Yii::$app->request->referrer,[
            'class'=>'btn btn-primary'
        ]);?>
    </div>
</div>



