<?php
use yii\helpers\Html;

?>

<h1>
    <?= $model->nombre; ?>
</h1>

<?= Html::img('@web/images/' . $model->foto, ['class' => 'img-responsive img-circle']) ?>

<?= Html::a('Obras de esta autor', ['autores/obras','id'=>$model->id], ['class' => 'btn btn-large btn-primary']); ?>

