<?php
use yii\widgets\ListView;

$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => ['listar']];
$this->params['breadcrumbs'][]= ' Listado de Autores';
        
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
]);


