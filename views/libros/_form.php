<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Libros */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="libros-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'editorial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'autor')->dropDownList(
        $autores,
        ['prompt'=>'Selecciona un autor...']
        );
    ?>

        <?= $form->field($model, 'portada')->fileInput() ?>

    <?php
    if (!is_null($model->portada)) {
        echo Html::img('@web/images/' . $model->portada, [
            'alt' => 'My logo',
            'width' => 800,
            'class' => 'img-responsive img-thumbnail'
        ]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
