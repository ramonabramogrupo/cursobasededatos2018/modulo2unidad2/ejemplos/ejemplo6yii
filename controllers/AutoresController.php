<?php

namespace app\controllers;

use Yii;
use app\models\Autores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AutoresController implements the CRUD actions for Autores model.
 */
class AutoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Autores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Autores::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Autores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Autores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Autores(); // crear objeto nuevo

        if ($model->load(Yii::$app->request->post())) {
            $model->foto = UploadedFile::getInstance($model, 'foto');
            if ($model->save()) {
                $model->upload();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }
    
    

    /**
     * Updates an existing Autores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
         
        if ($model->load(Yii::$app->request->post())) {
            // intentamos recuperar el fichero subido (actualizado)
            // en caso de que no seleccione ninguno el metodo estatico
            // devuelve null
            
            $puestoFoto=UploadedFile::getInstance($model, 'foto');
            if(is_null($puestoFoto)){
                // si no ha cogido ninguna foto cojo el valor anterior
                $model->foto=$model->getOldAttribute("foto");
            }else{
                $model->foto=$puestoFoto;
            }
            
            if ($model->save()) {
                // compruebo si hay que subir el archivo
                if(!is_null($puestoFoto)){
                    $model->upload();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Autores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Autores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Autores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Autores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionListar(){
        $dataProvider = new ActiveDataProvider([
            'query' => Autores::find(),
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);

        return $this->render('listar', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionObras($id){
        
        /*Autores::find()->joinWith("libros")
                ->where(['autor'=>$id]);*/
        
        
        $consulta=\app\models\Libros::find()->where(['autor'=>$id]);
        
       
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta,
            'pagination' => [
                'pageSize' => 2,
            ],
        ]);
        
        // opcion 1
        $autor=Autores::find()->where(['id'=>$id])->one();

        // opcion 2
        /*$modelo=$dataProvider->query->all(); // saco el modelo desde la consulta del dataprovider
        $modelo=$dataProvider->models;   // saco el modelo desde el dataprovider
        if(is_null($modelo)){
            echo "no tengo obras";
            $autor=Autores::find()->where(['id'=>$id])->one();
        }else{
            $autor=$modelo[0]->autor0; 
        }*/
        
        
       
        return $this->render('obrasautor', [
            'dataProvider' => $dataProvider,
            'nombre'=>$autor->nombre,
        ]);
    }
}
