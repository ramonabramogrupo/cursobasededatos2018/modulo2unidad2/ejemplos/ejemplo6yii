<?php

namespace app\controllers;

use Yii;
use app\models\Libros;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * LibrosController implements the CRUD actions for Libros model.
 */
class LibrosController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Libros models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Libros::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Libros model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Libros model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Libros();

        if ($model->load(Yii::$app->request->post())) {
            $model->portada = UploadedFile::getInstance($model, 'portada');
            if ($model->save()) {
                $model->upload();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $autores = \app\models\Autores::find()->all();
        $listData = ArrayHelper::map($autores, 'id', 'nombre');

        return $this->render('create', [
                    'model' => $model,
                    'autores' => $listData,
        ]);
    }

    /**
     * Updates an existing Libros model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            // intentamos recuperar el fichero subido (actualizado)
            // en caso de que no seleccione ninguno el metodo estatico
            // devuelve null
            
            $puestoFoto=UploadedFile::getInstance($model, 'portada');
            if(is_null($puestoFoto)){
                // si no ha cogido ninguna foto cojo el valor anterior
                $model->portada=$model->getOldAttribute("portada");
            }else{
                $model->portada=$puestoFoto;
            }
            
            if ($model->save()) {
                // compruebo si hay que subir el archivo
                if(!is_null($puestoFoto)){
                    $model->upload();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $autores = \app\models\Autores::find()->all();
        $listData = ArrayHelper::map($autores, 'id', 'nombre');

        return $this->render('update', [
                    'model' => $model,
                    'autores' => $listData,
        ]);
    }

    /**
     * Deletes an existing Libros model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Libros model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Libros the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Libros::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
